return {
  "neovim/nvim-lspconfig",
  dependencies = {
    "mfussenegger/nvim-jdtls",
  },
  opts = {
    setup = {
      handlers = {
        ["java/status"] = function(_, result)
          -- Print or whatever.
        end,
        ["$/progress"] = function(_, result, ctx)
          -- disable progress updates.
        end,
      },
    },
  },
}
