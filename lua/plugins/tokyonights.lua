return {
  "folke/tokyonight.nvim",
  opts = {
    transparent = true,
    styles = {
      sidebars = "transparent",
      floats = "transparent",
    },
  },
}, {
  require("notify").setup({
    background_colour = "#000000",
  }),
}
